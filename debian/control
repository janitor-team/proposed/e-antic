Source: e-antic
Section: math
Priority: optional
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 autoconf-archive, gnulib,
 libgmp-dev,
 libmpfr-dev,
 libantic-dev,
 libflint-dev,
 libflint-arb-dev,
 libboost-dev,
 libbenchmark-dev,
 libcereal-dev,
 catch2
Standards-Version: 4.6.0
Homepage: https://github.com/flatsurf/e-antic/
Vcs-Git: https://salsa.debian.org/math-team/e-antic.git
Vcs-Browser: https://salsa.debian.org/math-team/e-antic

Package: libeantic1
Provides: libeantic
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libeantic
Multi-Arch: same
Description: real Embedded Algebraic Number Theory In C/C++ - libs
 E-ANTIC is a C/C++ mathematical library to deal with real embedded
 number fields built on top of ANTIC; ANTIC is an Algebraic Number
 Theory library In C <https://github.com/wbhart/antic>. The aim of
 the E-ANTIC library is to have as fast as possible exact arithmetic
 operations and comparisons.
 .
 This package provides the shared libraries required to run programs compiled
 against the E-ANTIC library. To compile your own programs you also need to
 install the libeantic-dev package.

Package: libeantic-dev
Section: libdevel
Architecture: any
Depends:
 libgmp-dev,
 libmpfr-dev,
 libantic-dev,
 libflint-dev,
 libflint-arb-dev,
 libboost-dev,
 libeantic1 (= ${binary:Version}),
 ${misc:Depends}
Conflicts: libeantic-dev
Multi-Arch: same
Description: real Embedded Algebraic Number Theory In C/C++ - libdev
 E-ANTIC is a C/C++ mathematical library to deal with real embedded
 number fields built on top of ANTIC; ANTIC is an Algebraic Number
 Theory library In C <https://github.com/wbhart/antic>. The aim of
 the E-ANTIC library is to have as fast as possible exact arithmetic
 operations and comparisons.
 .
 This package contains static libraries and symbolic links that developers
 using the E-ANTIC library will need.
